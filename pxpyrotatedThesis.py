import kiteOM as kiteOAM
import numpy as np
import matplotlib.pyplot as plt
import pybinding as pb
from math import sqrt, pi
import argparse
import sys
import pylab as pl

parser = argparse.ArgumentParser(description='para incluir')

if(len(parser.parse_known_args()[1])!=10):
    print("Error: Incorrect number of parameters... The parameters are vppsigma, vpppi, soc, mass anderson disorder strength ebot etop size moments R\n")
    sys.exit()
else:
    pass

#declaration of the variables
##Slater Koster Hopping integrals
vppsigma = float(parser.parse_known_args()[1][0]) 
vpppi=float(parser.parse_known_args()[1][1])
#Spin Orbit coupling Constant
soc=float(parser.parse_known_args()[1][2])
#Sublattice Resolved Potential
mass=float(parser.parse_known_args()[1][3])
#Anderson Disorder Strength
anderson = float(parser.parse_known_args()[1][4])
###Spectrum Range for KPM
ebot = float(parser.parse_known_args()[1][5])
etop = float(parser.parse_known_args()[1][6])
#Number of Unit cells in both lattice vectors directions
size = int(parser.parse_known_args()[1][7])
##Chebyshev Polynomials
moments = int(parser.parse_known_args()[1][8])
##Random Vectors
R = int(parser.parse_known_args()[1][9])


def SKHoppings(vppsigma, vpppi, nnvector):
    n = np.multiply(1.0/sqrt(np.dot(nnvector,nnvector)),nnvector)
    aux = vppsigma-vpppi
    hoppings = [[0,0],[0,0]]
    x = n[0]
    y = n[1]
    xx = x*x
    yy = y*y
    hoppings[0][0] = xx*vppsigma + (1.0-xx)*vpppi
    hoppings[0][1] = x*y*aux
    hoppings[1][0] = x*y*aux
    hoppings[1][1] = yy*vppsigma + (1.0-yy)*vpppi
    return hoppings

#declaration of the lattice vectors
a0 =  0.535 #bismuthene unitary lattice length
a = a0/sqrt(3.0)
#lattice vectors
#V1
v1 = [1.0/2.0,sqrt(3.0)/2.0,0.0]
#V2
v2 = [-1.0/2.0,sqrt(3.0)/2.0,0.0]
v1 = np.multiply(a0,v1)
v2 = np.multiply(a0,v2)
#nearest neighbours
e1 = np.array([0.0,a,0.0])
e2 = np.add(-1.0*v1,e1)
e3 = np.add(-1.0*v2,e1)
#declaration of the nearest neighbours
#calculation of the hoppings
he1 = SKHoppings(vppsigma,vpppi,e1)
he2 = SKHoppings(vppsigma,vpppi,e2)
he3 = SKHoppings(vppsigma,vpppi,e3)

#rotated basis
Re1 = [[0,0],[0,0]]
Re2 = [[0,0],[0,0]]
Re3 = [[0,0],[0,0]]

#####LzBasisHoppings
#neighbour e1
Re1[0][0] = 0.5*(he1[0][0]+he1[1][1]+1.0j*(he1[0][1]-he1[1][0]))
Re1[1][1] = 0.5*(he1[0][0]+he1[1][1]+1.0j*(he1[1][0]-he1[0][1]))
Re1[1][0] = 0.5*(he1[0][0]-he1[1][1]+1.0j*(he1[0][1]+he1[1][0]))
Re1[0][1] = 0.5*(he1[0][0]-he1[1][1]-1.0j*(he1[0][1]+he1[1][0]))
#neighbour e2
Re2[0][0] = 0.5*(he2[0][0]+he2[1][1]+1.0j*(he2[0][1]-he2[1][0]))
Re2[1][1] = 0.5*(he2[0][0]+he2[1][1]+1.0j*(he2[1][0]-he2[0][1]))
Re2[1][0] = 0.5*(he2[0][0]-he2[1][1]+1.0j*(he2[0][1]+he2[1][0]))
Re2[0][1] = 0.5*(he2[0][0]-he2[1][1]-1.0j*(he2[0][1]+he2[1][0]))
#neighbour e3
Re3[0][0] = 0.5*(he3[0][0]+he3[1][1]+1.0j*(he3[0][1]-he3[1][0]))
Re3[1][1] = 0.5*(he3[0][0]+he3[1][1]+1.0j*(he3[1][0]-he3[0][1]))
Re3[1][0] = 0.5*(he3[0][0]-he3[1][1]+1.0j*(he3[0][1]+he3[1][0]))
Re3[0][1] = 0.5*(he3[0][0]-he3[1][1]-1.0j*(he3[0][1]+he3[1][0]))


####Declaration of the Pybinding lattice that will be used by KITE
def honeycombpxpy(soc,m):
    lattice = pb.Lattice(a1=[v1[0],v1[1]],a2=[v2[0],v2[1]])
    lattice.add_sublattices(
                            ('AUp',[0,0],m+soc),
                            ('ADown',[0,0],m-soc),
                            ('BUp',[0,-a],-m+soc),
                            ('BDown',[0,-a],-m-soc),)
    lattice.add_hoppings(
                         ([0,0],'BUp','AUp',Re1[0][0]),
                         ([0,0],'BDown','AUp',Re1[0][1]),
                         ([0,0],'BUp','ADown',Re1[1][0]),
                         ([0,0],'BDown','ADown',Re1[1][1]),
#                       second neighbours                            
                         ([-1,0],'BUp','AUp',Re2[0][0]),
                         ([-1,0],'BDown','AUp',Re2[0][1]),
                         ([-1,0],'BUp','ADown',Re2[1][0]),
                         ([-1,0],'BDown','ADown',Re2[1][1]),
#                       Third neighbours                            
                         ([0,-1],'BUp','AUp',Re3[0][0]),
                         ([0,-1],'BDown','AUp',Re3[0][1]),
                         ([0,-1],'BUp','ADown',Re3[1][0]),
                         ([0,-1],'BDown','ADown',Re3[1][1]),
                         )
    return lattice


####Definition of the Operator Lz used for the Orbital Current Calculation


def honeycombpxpyLz():
    lattice = pb.Lattice(a1=[v1[0],v1[1]],a2=[v2[0],v2[1]])
    lattice.add_sublattices(
                            ('AUp',[0,0],1),
                            ('ADown',[0,0],-1),
                            ('BUp',[0,-a],1),
                            ('BDown',[0,-a],-1),)
    return lattice

lattice = honeycombpxpy(soc,mass)

###Definition of the Disorder
disorder = kiteOAM.Disorder(lattice)
disorder.add_disorder('AUp','Uniform',+0.0,anderson*0.5)
disorder.add_disorder('ADown','Uniform',+0.0,anderson*0.5)
disorder.add_disorder('BUp','Uniform',+0.0,anderson*0.5)
disorder.add_disorder('BDown','Uniform',+0.0,anderson*0.5)
oam = honeycombpxpyLz()
###Number of Domains
nx=ny =2
lx=ly=size
###KiteConfiguration
configuration = kiteOAM.Configuration(divisions=[nx,ny],length=[lx,ly],boundaries=[True,True],is_complex=True,precision=0, spectrum_range=[ebot,etop])
calculation = kiteOAM.Calculation(configuration)
calculation.dos(num_points=10000,num_moments=moments,num_random=R,num_disorder=1)
###Instruction for the calculation of the ObitalProjected conductivity DC KITEop Only!!!!
#calculation.Opconductivity_dc(num_points=10000,num_moments=moments,num_random=R,num_disorder=1,direction='xy')
###Instruction fo rthe calculation of the charge conductivity DC
calculation.Opconductivity_dc(num_points=10000,num_moments=moments,num_random=R,num_disorder=1,direction='xy')
name = 'RotatedpxpyLzVppsigma'+str(vppsigma)+'Vpppi'+str(vpppi)+'SOC'+str(soc)+'VAB'+str(mass)+'Anderson'+str(anderson)+'size'+str(size)+'moments'+str(moments)+'RandomV'+str(R)+'.h5'
kiteOAM.config_system(lattice,configuration,calculation,filename=name,OM=oam, disorder=disorder)
