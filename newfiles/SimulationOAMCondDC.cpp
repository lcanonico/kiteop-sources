#include "Generic.hpp"
#include "ComplexTraits.hpp"
#include "myHDF5.hpp"
#include "Global.hpp"
#include "Random.hpp"
#include "Coordinates.hpp"
#include "LatticeStructure.hpp"
template <typename T, unsigned D>
class Hamiltonian;
template <typename T, unsigned D>
class KPM_Vector;
#include "queue.hpp"
#include "Simulation.hpp"
#include "Hamiltonian.hpp"
#include "KPM_VectorBasis.hpp"
#include "KPM_Vector.hpp"

template <typename T,unsigned D>
void Simulation<T,D>::calc_ampconddc(){
    debug_message("Entered Simulation::calc_AMPconddc\n");

    // Make sure that all the threads are ready before opening any files
    // Some threads could still be inside the Simulation constructor
    // This barrier is essential
#pragma omp barrier

  int NMoments, NRandom, NDisorder, direction;
  bool local_calculate_ampconddc = false;
#pragma omp master
{
  H5::H5File * file = new H5::H5File(name, H5F_ACC_RDONLY);
  Global.calculate_ampconddc = false;
  try{
    int dummy_variable;
    get_hdf5<int>(&dummy_variable,  file, (char *)   "/Calculation/AMP_conductivity_dc/NumMoments");
    Global.calculate_ampconddc = true;
  } catch(H5::Exception& e) {debug_message("AMPCondDC: no need to calculate AMPCondDC.\n");}
  file->close();
  delete file;
}
#pragma omp barrier
#pragma omp critical
  local_calculate_ampconddc = Global.ampcalculate_conddc;

#pragma omp barrier

if(local_calculate_ampconddc){
#pragma omp critical
{
    H5::H5File * file = new H5::H5File(name, H5F_ACC_RDONLY);

    debug_message("amp DC conductivity: checking if we need to calculate DC conductivity.\n");
    get_hdf5<int>(&direction, file, (char *) "/Calculation/AMP_conductivity_dc/Direction");
    get_hdf5<int>(&NMoments, file, (char *)  "/Calculation/AMP_conductivity_dc/NumMoments");
    get_hdf5<int>(&NRandom, file, (char *)   "/Calculation/AMP_conductivity_dc/NumRandoms");
    get_hdf5<int>(&NDisorder, file, (char *) "/Calculation/AMP_conductivity_dc/NumDisorder");

    file->close();
    delete file;

}
  ampCondDC(NMoments, NRandom, NDisorder, direction);
  }

}
template <typename T,unsigned D>

void Simulation<T,D>::ampCondDC(int NMoments, int NRandom, int NDisorder, int direction){
  std::string dir(num2str2(direction));
  std::string dirc = dir.substr(0,1)+","+dir.substr(1,2);
  Gamma2Dmod(NRandom, NDisorder, {NMoments,NMoments}, process_string(dirc), "/Calculation/AMP_conductivity_dc/Gamma"+dir);
}



template class Simulation<float ,1u>;
template class Simulation<double ,1u>;
template class Simulation<long double ,1u>;
template class Simulation<std::complex<float> ,1u>;
template class Simulation<std::complex<double> ,1u>;
template class Simulation<std::complex<long double> ,1u>;

template class Simulation<float ,3u>;
template class Simulation<double ,3u>;
template class Simulation<long double ,3u>;
template class Simulation<std::complex<float> ,3u>;
template class Simulation<std::complex<double> ,3u>;
template class Simulation<std::complex<long double> ,3u>;

template class Simulation<float ,2u>;
template class Simulation<double ,2u>;
template class Simulation<long double ,2u>;
template class Simulation<std::complex<float> ,2u>;
template class Simulation<std::complex<double> ,2u>;
template class Simulation<std::complex<long double> ,2u>;
