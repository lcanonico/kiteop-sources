set style data dots
set nokey
set xrange [0: 3.19444]
set yrange [-14.87253 :  6.77329]
set arrow from  1.16932, -14.87253 to  1.16932,   6.77329 nohead
set arrow from  1.84436, -14.87253 to  1.84436,   6.77329 nohead
set xtics ("G"  0.00000,"M"  1.16932,"K"  1.84436,"G"  3.19444)
 plot "mos2_band.dat"
