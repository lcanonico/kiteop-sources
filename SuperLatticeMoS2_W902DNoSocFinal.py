# This script gets the MoS2 data from dft+wannier90 for sp in S and d in Mo 
# and transforms it in a 2d pybinding lattice ->exports to kiteOM
# comparision between 2d/3d and DFT
import numpy as np
import matplotlib.pyplot as plt
import pybinding as pb
from math import pi
from math import sqrt
import kiteOM 
import argparse

parser = argparse.ArgumentParser(description='para incluir')
moments = int(parser.parse_known_args()[1][1])
size = int(parser.parse_known_args()[1][0])
random = int(parser.parse_known_args()[1][2])


####Class to read the Wannier90 Hamiltonian
class w90(object):
#imports w90 data
    def __init__(self, path, prefix):
        # store path and prefix
        self.path = path
        self.prefix = prefix

        # read in lattice_vectors
        f = open(self.path + "/" + self.prefix + ".win", "r")
        ln = f.readlines()
        f.close()
        # get lattice vector
        self.lat = np.zeros((3, 3), dtype=float)
        found = False
        for i in range(len(ln)):
            sp = ln[i].split()
            if len(sp) >= 2:
                if sp[0].lower() == "begin" and sp[1].lower() == "unit_cell_cart":
                    # get units right
                    if ln[i + 1].strip().lower() == "bohr":
                        pref = 0.5291772108
                        skip = 1
                    elif ln[i + 1].strip().lower() in ["ang", "angstrom"]:
                        pref = 1.0
                        skip = 1
                    else:
                        pref = 1.0
                        skip = 0
                    # now get vectors
                    for j in range(3):
                        sp = ln[i + skip + 1 + j].split()
                        for k in range(3):
                            self.lat[j, k] = float(sp[k]) * pref
                    found = True
                    break
        
        if not found:
            raise Exception("Unable to find unit_cell_cart block in the .win file.")

        # read in hamiltonian matrix, in eV
        f = open(self.path + "/" + self.prefix + "_hr.dat", "r")
        ln = f.readlines()
        f.close()
        #
        # get number of wannier functions
        self.num_wan = int(ln[1])
        # get number of Wigner-Seitz points
        num_ws = int(ln[2])
        # get degenereacies of Wigner-Seitz points
        deg_ws = []
        for j in range(3, len(ln)):
            sp = ln[j].split()
            for s in sp:
                deg_ws.append(int(s))
            if len(deg_ws) == num_ws:
                last_j = j
                break
            if len(deg_ws) > num_ws:
                raise Exception("Too many degeneracies for WS points!")
        deg_ws = np.array(deg_ws, dtype=int)
        # now read in matrix elements
        # Convention used in w90 is to write out:
        # R1, R2, R3, i, j, ham_r(i,j,R)
        # where ham_r(i,j,R) corresponds to matrix element < i | H | j+R >
        self.ham_r = {}  # format is ham_r[(R1,R2,R3)]["h"][i,j] for < i | H | j+R >
        ind_R = 0  # which R vector in line is this?
        for j in range(last_j + 1, len(ln)):
            sp = ln[j].split()
            # get reduced lattice vector components
            ham_R1 = int(sp[0])
            ham_R2 = int(sp[1])
            ham_R3 = int(sp[2])
            # get Wannier indices
            ham_i = int(sp[3]) - 1
            ham_j = int(sp[4]) - 1
            # get matrix element
            ham_val = float(sp[5]) + 1.0j * float(sp[6])
            # store stuff, for each R store hamiltonian and degeneracy
            ham_key = (ham_R1, ham_R2, ham_R3)
            if not (ham_key in self.ham_r):
                self.ham_r[ham_key] = {
                    "h": np.zeros((self.num_wan, self.num_wan), dtype=complex),
                    "deg": deg_ws[ind_R]
                }
                ind_R += 1
            self.ham_r[ham_key]["h"][ham_i, ham_j] = ham_val

        # check if for every non-zero R there is also -R
        for R in self.ham_r:
            if not (R[0] == 0 and R[1] == 0 and R[2] == 0):
                found_pair = False
                for P in self.ham_r:
                    if not (R[0] == 0 and R[1] == 0 and R[2] == 0):
                        # check if they are opposite
                        if R[0] == -P[0] and R[1] == -P[1] and R[2] == -P[2]:
                            if found_pair:
                                raise Exception("Found duplicate negative R!")
                            found_pair = True
                if not found_pair:
                    raise Exception("Did not find negative R for R = " + R + "!")

        # read in wannier centers
        f = open(self.path + "/" + self.prefix + "_centres.xyz", "r")
        ln = f.readlines()
        f.close()
        # Wannier centers in Cartesian, Angstroms
        xyz_cen = []
        for i in range(2, 2 + self.num_wan):
            sp = ln[i].split()
            if sp[0] == "X":
                tmp = []
                for j in range(3):
                    tmp.append(float(sp[j + 1]))
                xyz_cen.append(tmp)
            else:
                raise Exception("Inconsistency in the centres file.")
        self.xyz_cen = np.array(xyz_cen, dtype=float)
        # get orbital positions in reduced coordinates
        # self.red_cen = _cart_to_red((self.lat[0], self.lat[1], self.lat[2]), self.xyz_cen)

###Funciton to export the W90 Hamiltonian to pybinding hmax is the abs of the smallest hopping
def lattice_from_w90(mat_lattice, hmax=100):
    num_orbitals = mat_lattice.num_wan

    onsite_orb = np.zeros(num_orbitals)

    ham = (0, 0, 0)

    hamiltonian = mat_lattice.ham_r[ham]['h']
    for idx in range(hamiltonian.shape[0]):
        onsite_orb[idx] += hamiltonian[idx, idx].real / float(mat_lattice.ham_r[ham]['deg'])

    lat = pb.Lattice(a1=mat_lattice.lat[0], a2=mat_lattice.lat[1], a3=mat_lattice.lat[2])
    for idx, site in enumerate(mat_lattice.xyz_cen):
        lat.add_one_sublattice('X' + str(idx), site, onsite_orb[idx])

    already_added_ham = []
    for ham in mat_lattice.ham_r:

        if not (ham in already_added_ham) and not ((-ham[0], -ham[1], -ham[2]) in already_added_ham):
            hamiltonian = mat_lattice.ham_r[ham]['h']
            degeneracy = float(mat_lattice.ham_r[ham]['deg'])
            for idx_i in range(hamiltonian.shape[0]):
                for idx_j in range(hamiltonian.shape[1]):
                    
                    if np.sum(np.square(np.asarray(ham))) == 0:
                        if idx_i < idx_j:
                           if np.abs(hamiltonian[idx_j, idx_i] / degeneracy)>hmax:
                               lat.add_one_hopping(ham, 'X' + str(idx_i), 'X' + str(idx_j),
                                                hamiltonian[idx_j, idx_i] / degeneracy)
                    else:
                        if np.abs(hamiltonian[idx_j, idx_i] / degeneracy)>hmax:
                           lat.add_one_hopping(ham, 'X' + str(idx_i), 'X' + str(idx_j),
                                            hamiltonian[idx_j, idx_i] / degeneracy)

            already_added_ham.append(ham)
    return lat

###W90 Folders
folder = r"W90_data/mos2/"
seedname = r"mos2"
mos2_lattice = w90(folder, seedname)

#genetates 2d lattice from 3d where S orbitals move to z=0

lat = lattice_from_w90(mos2_lattice,0.025)

def rewriteidx(idx):
    arr = np.abs(idx)/4 #condition to fit inside the cells
    #preparation of the index i
    if(arr[0]<0.5):
        arr[0] = int(np.floor(arr[0]))
    else:
        arr[0] = int(np.ceil(arr[0]))
    #preparation of the index j
    if(arr[1]<0.5):
        arr[1] = int(np.floor(arr[1]))
    else:
        arr[1] = int(np.ceil(arr[1]))
    #desicion to determine in which lattice the site is
    if(arr[0] == 0):
        arr[0] = np.floor(np.abs(idx[0])/2)
    else:
        pass
    if(arr[1] == 0):
        arr[1] = np.floor(np.abs(idx[1])/2)
    else:
        pass
    #setting the sign of the lattice
    arr[0] =int(np.sign(idx[0])*arr[0])
    arr[1] = int(np.sign(idx[1])*arr[1])
    return np.asarray(arr)

#######this section of the code generates 2d lattices from the 3d one, where S oorbitals move to z=0 and the unit cell is now 3x larger


######################2D part

a12d = np.asarray(lat.vectors[0][0:2])
a22d = np.asarray(lat.vectors[1][0:2])

Sa1 = 3*a12d
Sa2 = 3*a22d

#creation of the lattice

lat2d = pb.Lattice(a1=Sa1, a2=Sa2)
lat2dOrbital = pb.Lattice(a1=Sa1, a2=Sa2)

#####insertion of the sublattices

for m in range(-1,2):
    for n in range(-1,2):
        for name, sub in lat.sublattices.items():
            if True:
                new_name = str(name)+'_'+str(m)+str(n)
                new_pos = np.asarray(sub.position[0:2]+(m*a12d)+(n*a22d))
                lat2d.add_one_sublattice(new_name, new_pos,np.real(sub.energy[0][0]))
                lat2dOrbital.add_one_sublattice(new_name, new_pos)
                new_name = []
                new_pos = []
                

#####array to store the positions of the atoms contained in the superlattice

scellidx = [np.asarray([m,n]) for m in range(-1,2) for n in range(-1,2)]
#####section to add the hoppings inside the unit cell
for m in range(-1,2):
    for n in range(-1,2):
        for name, hop in lat.hoppings.items():
            for term in hop.terms:
                if True:
                    cell = np.array(term.relative_index[0:2])
                    newidx = np.add(cell,np.asarray([m,n]))
                    slatidx = rewriteidx(newidx)
                    ref = int(3)*slatidx
                    lat2d.add_one_hopping(slatidx,'X'+str(term.from_id)+'_'+str(m)+str(n),'X'+str(term.to_id)+'_'+str(int(m+cell[0]-ref[0]))+str(int(n+cell[1]-ref[1])),hop.energy[0][0])



#####creation of the lattice for the OHE calculation

for m in range(-1,2):
    for n in range(-1,2):
        lat2dOrbital.add_one_hopping([0,0],'X4'+'_'+str(m)+str(n),'X3'+'_'+str(m)+str(n),-2j)
        lat2dOrbital.add_one_hopping([0,0],'X2'+'_'+str(m)+str(n),'X1'+'_'+str(m)+str(n),-1j)
        lat2dOrbital.add_one_hopping([0,0],'X7'+'_'+str(m)+str(n),'X8'+'_'+str(m)+str(n),1j)
        lat2dOrbital.add_one_hopping([0,0],'X11'+'_'+str(m)+str(n),'X12'+'_'+str(m)+str(n),1j)





#####KITE section


## Number of Domains
nx = 4
ny = 4

####system size

l1 =  size
l2 = size

energy_scale = 15.5

configuration = kiteOM.Configuration(divisions=[nx,ny], length=[l1, l2], boundaries=[True, True], is_complex=True, precision=1, spectrum_range=[-energy_scale, energy_scale])

calculation = kiteOM.Calculation(configuration)
calculation.dos(num_moments=moments, num_random=random, num_disorder=1,num_points=1000)
calculation.Opconductivity_dc(num_moments=moments, num_random=random, num_disorder=1, num_points=1000,temperature=100.0, direction='xy')
name = 'SlatMoS2W90NoSOCSize'+str(size)+'x'+str(size)+'Moments'+str(moments)+'Random'+str(random)+'.h5'
kiteOM.config_system(lat2d, configuration, calculation, OM=lat2dOrbital,filename=name)

                
