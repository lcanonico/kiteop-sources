"""       
        ##############################################################################      
        #                        KITE | Pre-Release version 0.1                      #      
        #                                                                            #      
        #                        Kite home: quantum-kite.com                         #           
        #                                                                            #      
        #  Developed by: Simao M. Joao, Joao V. Lopes, Tatiana G. Rappoport,         #       
        #  Misa Andelkovic, Lucian Covaci, Aires Ferreira, 2018                      #      
        #                                                                            #      
        ##############################################################################      
"""
""" Onsite disorder

    Lattice : Monolayer graphene;
    Disorder : Disorder class Deterministic and Uniform at different sublattices,
    Configuration : size of the system 512x512, without domain decomposition (nx=ny=1), periodic boundary conditions,
                    double precision, manual scaling;
    Calculation : dos;
"""

import kiteOM as kite
#from pybinding.repository import graphene
import pybinding as pb
import group6_tmdsoc as tmd
#from pybinding.repository import group6_tmd as tmd
import matplotlib.pyplot as plt
# load graphene lattice and structural_disorder
lattice = tmd.monolayer_3band_soc("MoS2")
orbital = tmd.monolayer_3band_sz("MoS2")
#lattice.plot()
model = pb.Model(lattice, pb.translational_symmetry())
solver = pb.solver.lapack(model)
k_points = model.lattice.brillouin_zone()
gamma = [0, 0]
k = k_points[0]
m = (k_points[0] + k_points[1]) / 2
plt.figure(figsize=(6.7, 2.3))
plt.subplot(121, title="MoS2 3-band model band structure")
bands = solver.calc_bands(gamma, k, m, gamma)
bands.plot(point_labels=[r"$\Gamma$", "K", "M", r"$\Gamma$"])
#plt.show()

# add Disorder
disorder = kite.Disorder(lattice)
#disorder.add_disorder('B', 'Deterministic', -1.0)
#disorder.add_disorder("Mo", 'Uniform',0, 0.01)
# number of decomposition parts in each direction of matrix.
# This divides the lattice into various sections, each of which is calculated in parallel
nx = ny = 1
# number of unit cells in each direction.
lx = ly = 128
# make config object which caries info about
# - the number of decomposition parts [nx, ny],
# - lengths of structure [lx, ly]
# - boundary conditions, setting True as periodic boundary conditions, and False elsewise,
# - info if the exported hopping and onsite data should be complex,
# - info of the precision of the exported hopping and onsite data, 0 - float, 1 - double, and 2 - long double.
# - scaling, if None it's automatic, if present select spectrum_bound=[e_min, e_max]
configuration = kite.Configuration(divisions=[nx, ny], length=[lx, ly], boundaries=[True, True],
                                   is_complex=True,spectrum_range=[-4.5, 4.5], precision=1)
# require the calculation of DOS
calculation = kite.Calculation(configuration)
calculation.dos(num_points=4000, num_moments=128, num_random=10, num_disorder=1)
calculation.Opconductivity_dc(num_points=6000, num_moments=128, num_random=1, num_disorder=1,direction='xy', temperature=100)
# configure the *.h5 file
kite.config_system(lattice, configuration, calculation,OM=orbital, filename='tmds.h5')

