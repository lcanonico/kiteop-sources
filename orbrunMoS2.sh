size=256
moments=512
random=8
ebot=-4.5
etop=4.5
for i in {0..0}
	do
		python3 tmds_orb.py ${ebot} ${etop} ${size} ${moments} ${random}
		mv MoS2OrbitalHallSize${size}x${size}Moments${moments}RandomV${random}.h5  MoS2OrbitalHallSize${size}x${size}Moments${moments}RandomV${random}Run${i}.h5
		./KITEx MoS2OrbitalHallSize${size}x${size}Moments${moments}RandomV${random}Run${i}.h5
	       ./KITE-tools MoS2OrbitalHallSize${size}x${size}Moments${moments}RandomV${random}Run${i}.h5 --OpCondDC -F -5.5 5.5 11535 -E 11535 -N MoS2OrbitalHallSize${size}x${size}Moments${moments}RandomV${random}Run${i}.dat -S 0.01
	       mv dos.dat DOSMoS2OrbitalHallSize${size}x${size}Moments${moments}RandomV${random}Run${i}.dat
	done 
