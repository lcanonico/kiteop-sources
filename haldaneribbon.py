"""       
        ##############################################################################      
        #                        KITE | Pre-Release version 0.1                      #      
        #                                                                            #      
        #                        Kite home: quantum-kite.com                         #           
        #                                                                            #      
        #  Developed by: Simao M. Joao, Joao V. Lopes, Tatiana G. Rappoport,         #       
        #  Misa Andelkovic, Lucian Covaci, Aires Ferreira, 2018                      #      
        #                                                                            #      
        ##############################################################################      
"""
""" DOS and DC conductivity of the Haldane model

    Lattice : Honeycomb lattice;
    Disorder : Disorder class Uniform at different sublattices;
    Configuration : size of the system 256x256, with domain decomposition (nx=ny=1), periodic boundary conditions,
                    double precision, automatic scaling;
    Calculation : DOS and conductivity_dc (xy);
"""

import pybinding as pb
import numpy as np
import matplotlib.pyplot as plt
from math import sqrt, pi


def haldane():
    """Return the lattice specification for Haldane model"""

    a = 0.24595   # [nm] unit cell length
    a_cc = 0.142  # [nm] carbon-carbon distance

    t=-1
    t2 = 0/10
    m=0.
    # create a lattice with 2 primitive vectors
    lat = pb.Lattice(
        a1=[a, 0],
        a2=[a/2, a/2 * sqrt(3)]
    )

    lat.add_sublattices(
        # name and position
        ('A', [0, -a_cc/2],-m),
        ('B', [0,  a_cc/2],m)
    )

    lat.add_hoppings(
        # inside the main cell
        ([0,  0], 'A', 'B', t),
        # between neighboring cells
        ([1, -1], 'A', 'B', t),
        ([0, -1], 'A', 'B', t),
        ([1, 0], 'A', 'A', t2 * 1j),
        ([0, -1], 'A', 'A', t2 * 1j),
        ([-1, 1], 'A', 'A', t2 * 1j),
        ([1, 0], 'B', 'B', t2 * -1j),
        ([0, -1], 'B', 'B', t2 * -1j),
        ([-1, 1], 'B', 'B', t2 * -1j)
    )

    return lat


lattice = haldane()

model = pb.Model(lattice, pb.primitive(a2=8),pb.translational_symmetry(a1=True,a2=False))

solver = pb.solver.lapack(model)

a = 0.24595   # [nm] unit cell length
bands = solver.calc_bands([-pi/a,0],[0,0],[pi/a,0])
bands.plot(point_labels=[r'$\frac{-\pi}{a_0}$','0',r'$\frac{\pi}{a_0}$'])
plt.show()

