######This is an include file that contains the script used to pass the wannier information to pybinding
import numpy as np
from math import pi,sqrt
import numpy as np
import pybinding as pb


class w90():
#import wannier data
    def __init__(self, path, prefix):
        #store the path and prefix inside the class
        self._path = path
        self._prefix = prefix
        ### read the lattice vectors
        f = open(self._path+"/"+self._prefix+".win","r")
        ln = f.readlines()
        f.close()
        ###import lattice vectors
        self._lat = np.zeros((3,3),dtype=float)
        found = False
        for i in range(len(ln)):
            sp = ln[i].split()
            if len(sp) >= 2:
                if sp[0].lower() == "begin" and sp[1].lower() == "unit_cell_cart":
                    #get units right
                    if ln[i+1].strip().lower() == 'bohr':
                        #pref = 0.5291772108 #to get the units in angstrom
                        pref = 0.05291772108 #to get the units in nanometers 
                        skip =1
                    elif ln[i+1].strip().lower() in ['ang','angstrom']:
                        pref = 1.0
                        skip = 1
                    else:
                        pref = 1.0
                        skip = 0
                    #now get the vectors
                    for j in range(3):
                        sp = ln[i+skip+1+j].split()
                        for k in range(3):
                            self._lat[j,k] = float(sp[k])*pref
                    found = True
                    break
        if not found:
            raise Exception("Error: Unable to find the unit_cell_cart block in the .win file")
        
        # read in hamiltonian matrix, in eV
        f = open(self._path+"/"+self._prefix+"_hr.dat","r")
        ln = f.readlines()
        f.close()
        #
        # get the number of wannier functions
        self._num_wan = int(ln[1])
        #get the number of Wigner-Sitz points
        num_ws = int(ln[2])
        #get degeneracies of Wigner-Seitz points
        deg_ws = []
        for j in range(3, len(ln)):
            sp = ln[j].split()
            for s in sp:
                deg_ws.append(int(s))
            if len(deg_ws) == num_ws:
                last_j = j
                break
            if len(deg_ws) > num_ws:
                raise Exception("Error: Too many degeneracies for WS points!")
        
        deg_ws = np.array(deg_ws,dtype=int)
        # now read in matrix elements
        # Convention used in w90 is to write out:
        # R1, R2, R3, i, j, ham_r(i,j,R)
        # where ham_r(i,j,R) corresponds to matrix element < i | H | j+R >
        self._ham_r = {}  # format is ham_r[(R1,R2,R3)]["h"][i,j] for < i | H | j+R >
        ind_R = 0  # which R vector in line is this?
        for j in range(last_j + 1, len(ln)):
            sp = ln[j].split()
            #get reduced lattice vector components
            ham_R1 = int(sp[0])
            ham_R2 = int(sp[1])
            ham_R3 = int(sp[2])
            #get wannier indices
            ham_i = int(sp[3]) - 1
            ham_j = int(sp[4]) - 1
            #get matrix element
            ham_val = float(sp[5]) + 1.0j*float(sp[6])
            #store stuff, for each R store hamiltonian and degeneracy
            ham_key = (ham_R1, ham_R2, ham_R3)
            if not(ham_key in self._ham_r):
                self._ham_r[ham_key] = {"h": np.zeros((self._num_wan,self._num_wan), dtype=complex), "deg": deg_ws[ind_R]}
                ind_R += 1
            self._ham_r[ham_key]["h"][ham_i, ham_j] = ham_val

        #check if for every non-zero R there is also -R
        for R in self._ham_r:
            if not (R[0]==0 and R[1]== 0 and R[2]==0):
                found_pair = False
                for P in self._ham_r:
                    if not (R[0]==0 and R[1]==0 and R[2]==0):
                        #check if they are opposite
                        if R[0] == -P[0] and R[1] == -P[1] and R[2] == -P[2]:
                            if found_pair:
                                raise Exception("Error: Found duplicate negative R!")
                            found_pair = True
                if not found_pair:
                    raise Exception("Did not find negative R for R = "+ R + "!")
        
        #read Wannier Centers
        f = open(self._path+"/"+self._prefix+"_centres.xyz","r")
        ln = f.readlines()
        f.close()
        #Wannier Centres in Cartesian, Angstroms
        xyz_cen = []
        for i in range(2,2+self._num_wan):
            sp = ln[i].split()
            if sp[0] == "X":
                tmp = []
                for j in range(3):
                    tmp.append(float(sp[j+1]))
                xyz_cen.append(tmp)
            else:
                raise Exception("Inconsistency in the centres file.")
        self._xyz_cen = np.array(xyz_cen,dtype=float)

    def lattice_from_w90(self,hmax=100):
    #exports into pybinding lattice. Hmax is the abs of the maximum hopping value
        num_orbitals = self._num_wan
        onsite_orb = np.zeros(num_orbitals)
        ham = (0,0,0)
        hamiltonian = self._ham_r[ham]['h']
        for idx in range(hamiltonian.shape[0]):
            onsite_orb[idx] += hamiltonian[idx,idx].real/float(self._ham_r[ham]['deg'])
        lat = pb.Lattice(a1=self._lat[0],a2=self._lat[1],a3=self._lat[2])
        for idx,site in enumerate(self._xyz_cen):
            lat.add_one_sublattice('X'+str(idx),site,onsite_orb[idx])
        
        already_added_ham = []
        for ham in self._ham_r:
            if not (ham in already_added_ham) and not ((-ham[0], -ham[1], -ham[2]) in already_added_ham):
                hamiltonian = self._ham_r[ham]['h']
                degeneracy = float(self._ham_r[ham]['deg'])
                for idx_i in range(hamiltonian.shape[0]):
                    for idx_j  in range(hamiltonian.shape[1]):
                        if np.sum(np.square(np.asarray(ham))) == 0:
                            if idx_i < idx_j:
                                if np.abs(hamiltonian[idx_j,idx_i]/degeneracy) > hmax:
                                    lat.add_one_hopping(ham,'X'+str(idx_i),'X'+str(idx_j),hamiltonian[idx_j,idx_i]/degeneracy)
                        
                        else:
                            if np.abs(hamiltonian[idx_j,idx_i]/degeneracy)>hmax:
                                lat.add_one_hopping(ham,'X'+str(idx_i),'X'+str(idx_j),hamiltonian[idx_j,idx_i]/degeneracy)
            
            already_added_ham.append(ham)
        return lat


