import h5py
import numpy as np
#file_name = 'haldaneOAM.h5'
file_name = 'rotatedpxpyVppsigma1.51522Vpppi-0.575788SOC0.435VAB0.0Anderson0.05.h5'
f = h5py.File(file_name, 'r+')     # open the file

# List all groups
print('All groups')
for key in f.keys():  # Names of the groups in HDF5 file.
    print(key)
print()

# Get the HDF5 group
group = f['OAM']
print(group)
# Checkout what keys are inside that group.
print('Single group')
for key in group.keys():
    print(key)
#print()
#if you want to modify other quantity, check de list and change the subgroup below
# Get the HDF5 subgroup
subgroup = group['d']
print(subgroup)
dat = np.array(subgroup)
print(dat)
subgroup = group['Hoppings']
print(subgroup)
dat = np.array(subgroup)
print(dat)
subgroup = group['NHoppings']
print(subgroup)
dat = np.array(subgroup)
print(dat)
subgroup = group['StructuralDisorder']
print(subgroup)
dat = np.array(subgroup)
print(dat)
subgroup = group['Vacancy']
print(subgroup)
dat = np.array(subgroup)
subgroup = group['Disorder']
print(subgroup)
dat = np.array(subgroup)

print(dat)




'''
new_value = 70
data = subgroup['Temperature']  # load the data
data[...] = new_value  # assign new values to data
f.close()  # close the file

# To confirm the changes were properly made and saved:

f1 = h5py.File(file_name, 'r')
print(np.allclose(f1['Calculation/AMP_conductivity_dc/Temperature'].value, new_value))
'''
